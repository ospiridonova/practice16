package com.services;

import com.exceptions.WrongFileException;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Olesya on 06.04.2017.
 */
public class ParserService {

    private final String filePath;

    private ArrayList<String> numbers;

    private static Logger logger = Logger.getLogger(ParserService.class);

    static {
        DOMConfigurator.configure("log4j.xml");
    }

    public ArrayList<String> getNumbers() {
        return numbers;
    }

    public String getFilePath() {
        return filePath;
    }

    /**
     * Конструктор - создание нового объекта
     * Разбивает полученный файл на слова
     * и добавляет их в массив numbers
     *
     * @see ParserService#ParserService(String)
     */
    public ParserService(String filePath) throws IOException, WrongFileException {

        this.filePath = filePath;
        InputStream stream;

        if (filePath.contains("http://") || filePath.contains("https://")) {
            URL myURL = new URL(filePath);
            stream = myURL.openStream();
        } else {
            stream = new FileInputStream(filePath);
        }
        BufferedReader buffReader = new BufferedReader(new InputStreamReader(stream));
        parseReader(buffReader);
    }

    /**
     * <p>Разделяет текст на слова и сохраняет их в numbers</p>*
     * @throws WrongFileException если в файле встречается неразрешенный символ
     * @throws IOException        если есть проблемы с чтением файла
     */
    private void parseReader(BufferedReader buffReader) throws IOException, WrongFileException {
        numbers = new ArrayList<>();
        while (buffReader.ready()) {

            for (String str : buffReader.readLine().split("\\s+")) {
                if (isNumber(str)) {
                    if (!("".equals(str))) {
                        numbers.add(str);
                    }

                } else {
                    buffReader.close();
                    throw new WrongFileException("Текст \"" + getFilePath() + "\" содержит некорректные символы!");
                }
            }
        }
        buffReader.close();
        logger.trace(filePath + " " + numbers.size() + " чисел");
    }

    /**
     * <p>Проверяет, является ли строка валидной</p>
     * Строка валидна, если в ней содержатся только цифры и унарный оператор "-"
     * @param str Проверяемая строка
     * @return true, если входная строка допустима, и false, если недопустима
     */
    public static boolean isNumber(String str) {
        Pattern pattern = Pattern.compile("^-?[0-9]\\d*(\\.\\d+)?$");
        Matcher matcher = pattern.matcher(str);
        return matcher.matches();
    }
}

