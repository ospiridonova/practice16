package com.test;

import com.models.NumberCollection;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Olesya on 10.04.2017.
 */
public class NumberCollectionTest {

    private static NumberCollection numberCollection;

    /**
     * Инициализация numberCollection
     */
    @BeforeAll
    public static void init() {
        numberCollection = new NumberCollection();
    }

    /**
     * Метод для тестирования суммы четных чисел коллекции
     * с использованием синхронизированного метода
     */
    @Test
    void testPutSynchronized() {
        assertNotNull(numberCollection);
        for (long number = -5; number <= 5; number++) {
            numberCollection.putSynchronized(String.valueOf(number));
        }
        assertTrue(numberCollection.calculateSum("-5") == 0);
        assertTrue(numberCollection.calculateSum("-4") == -4);
        assertTrue(numberCollection.calculateSum("-3") == -4);
        assertTrue(numberCollection.calculateSum("-2") == -6);
        assertTrue(numberCollection.calculateSum("-1") == -6);
        assertTrue(numberCollection.calculateSum("0") == -6);
        assertTrue(numberCollection.calculateSum("1") == -6);
        assertTrue(numberCollection.calculateSum("2") == -4);
        assertTrue(numberCollection.calculateSum("3") == -4);
        assertTrue(numberCollection.calculateSum("4") == 0);
        assertTrue(numberCollection.calculateSum("5") == 0);
    }


    /**
     * Метод для тестирования суммы четных чисел коллекции
     * с использованием lock-а
     */
    @Test
    void testPutWithReentrantLock() {
        assertNotNull(numberCollection);
        for (long number = -5; number <= 5; number++) {
            numberCollection.putWithReentrantLock(String.valueOf(number));
        }
        assertTrue(numberCollection.calculateSum("-5") == 0);
        assertTrue(numberCollection.calculateSum("-4") == -4);
        assertTrue(numberCollection.calculateSum("-3") == -4);
        assertTrue(numberCollection.calculateSum("-2") == -6);
        assertTrue(numberCollection.calculateSum("-1") == -6);
        assertTrue(numberCollection.calculateSum("0") == -6);
        assertTrue(numberCollection.calculateSum("1") == -6);
        assertTrue(numberCollection.calculateSum("2") == -4);
        assertTrue(numberCollection.calculateSum("3") == -4);
        assertTrue(numberCollection.calculateSum("4") == 0);
        assertTrue(numberCollection.calculateSum("5") == 0);
    }
}
