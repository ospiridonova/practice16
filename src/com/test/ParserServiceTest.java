package com.test;

import com.exceptions.WrongFileException;
import com.models.NumberCollection;
import com.services.ParserService;
import com.utils.UtilService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Olesya on 10.04.2017.
 */
public class ParserServiceTest {

    private static NumberCollection numberCollection;
    private static final String TEST_FILE_NAME = "test_numbers.txt";
    private static final String TEST_FILE_NAME_WITH_ERROR = "test_error_numbers.txt;";
    private static final String TEST_WRONG_FILE_NAME = "wrong_file_name";

    /**
     * Создаем файлик с числами от -1 до 5
     */
    @BeforeAll
    public static void init() {
        //создать хороший файлик
        UtilService utilService = new UtilService();
        utilService.generateTestFile(TEST_FILE_NAME, false);
        utilService.generateTestFile(TEST_FILE_NAME_WITH_ERROR, true);
        numberCollection = new NumberCollection();
    }

    /**
     * Метод для тестирования возвращаемого файлика
     */
    @Test
    void testTakenFile() {
        try {
            ParserService ps = new ParserService(TEST_FILE_NAME);
            assertTrue(TEST_FILE_NAME.equals(ps.getFilePath()));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (WrongFileException wrongFileException) {
            wrongFileException.printStackTrace();
        }
    }

    /**
     * Метод тестирования считанного массива чисел
     * @throws IOException
     * @throws WrongFileException
     */
    @Test
    void testNumberEquals() throws IOException, WrongFileException {
        ArrayList<String> testNumbers = new ArrayList<>();
        for (int number = -5; number <= 5; number++) {
            testNumbers.add(String.valueOf(number));
        }
        ArrayList<String> numbers = new ParserService(TEST_FILE_NAME).getNumbers();
        assertArrayEquals(testNumbers.toArray(), numbers.toArray());
    }

    /**
     * Метод тестирования поведения парсера в случае файлика с некорректным содержимым
     */
    @Test
    public void testFileWithError() {
        assertThrows(WrongFileException.class, () -> new ParserService(TEST_FILE_NAME_WITH_ERROR));
    }

    /**
     * Метод тестирования поведения парсера в случае передачи некорректного filePath
     */
    @Test
    public void testFileWithWrongPath() {
        assertThrows(IOException.class, () -> new ParserService(TEST_WRONG_FILE_NAME));
    }
}
