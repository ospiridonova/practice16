package com.test;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.models.NumberCollection;
import com.models.ThreadModel;
import com.utils.UtilService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Olesya on 10.04.2017.
 */
public class ThreadModelTest {


    private static NumberCollection numberCollection;
    private static final String TEST_FILE_NAME1 = "test_number_1.txt";
    private static final String TEST_FILE_NAME2 = "test_number_2.txt";
    private static final String TEST_FILE_NAME3 = "test_number_3.txt";
    private static final String TEST_FILE_NAME_WITH_ERROR = "test_error_numbers.txt;";
    private static final String TEST_WRONG_FILE_NAME = "wrong_file_name";

    /**
     * Создаем файлик с числами от -1 до 5
     */
    @BeforeAll
    public static void init() {
        /*
        создаем три тестовых файлика для тестрования трех потоков
         */
        UtilService utilService = new UtilService();
        utilService.generateTestFile(TEST_FILE_NAME1, false);
        utilService.generateTestFile(TEST_FILE_NAME2, false);
        utilService.generateTestFile(TEST_FILE_NAME3, false);
        numberCollection = new NumberCollection();
    }

    @Test
    void testCall() {
        ExecutorService es = Executors.newFixedThreadPool(3);
        long start = System.currentTimeMillis();
        ArrayList<Future<Long>> list = new ArrayList<Future<Long>>(); //результирующий
        Future<Long> submit1 = es.submit(new ThreadModel(TEST_FILE_NAME1, numberCollection));
        list.add(es.submit(new ThreadModel(TEST_FILE_NAME1, numberCollection)));
        Future<Long> submit2 = es.submit(new ThreadModel(TEST_FILE_NAME2, numberCollection));
        list.add(es.submit(new ThreadModel(TEST_FILE_NAME2, numberCollection)));
        Future<Long> submit3 = es.submit(new ThreadModel(TEST_FILE_NAME3, numberCollection));
        list.add(es.submit(new ThreadModel(TEST_FILE_NAME3, numberCollection)));
        es.shutdown();
        assertTrue(numberCollection.calculateSum("-5") == 0);
        assertTrue(numberCollection.calculateSum("-4") == -4);
        assertTrue(numberCollection.calculateSum("-3") == -4);
        assertTrue(numberCollection.calculateSum("-2") == -6);
        assertTrue(numberCollection.calculateSum("-1") == -6);
        assertTrue(numberCollection.calculateSum("0") == -6);
        assertTrue(numberCollection.calculateSum("1") == -6);
        assertTrue(numberCollection.calculateSum("2") == -4);
        assertTrue(numberCollection.calculateSum("3") == -4);
        assertTrue(numberCollection.calculateSum("4") == 0);
        assertTrue(numberCollection.calculateSum("5") == 0);
    }
}
