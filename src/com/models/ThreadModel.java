package com.models;

import com.services.ParserService;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import java.util.ArrayList;
import java.util.concurrent.Callable;

/**
 * Created by Olesya on 06.04.2017.
 */
public class ThreadModel implements Callable<Long> {

    private String filePath;

    private NumberCollection numberCollection;

    private volatile static boolean error = false;

    private static Logger logger = Logger.getLogger(ThreadModel.class);

    static {
        DOMConfigurator.configure("log4j.xml");
    }

    public ThreadModel(String filePath, NumberCollection numberCollection) {
        this.filePath = filePath;
        this.numberCollection = numberCollection;
    }

    public static void setError(boolean error) {
        ThreadModel.error = error;
    }

    @Override
    public Long call() throws Exception {
        System.out.println("старт потока:" + filePath);
        logger.trace("старт потока");
        ArrayList<String> words = null;
        Long currentSum = 0L;
        try {
            words = new ParserService(filePath).getNumbers();
            for (String str : words) {
                //return numberCollection;
                if (!error) {
                    currentSum = numberCollection.putSynchronized(str);
                } else break;
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            ThreadModel.setError(true);
            System.out.println(e.getMessage());
        }
        System.out.println("завершение потока:" + filePath);
        logger.trace("завершение потока");
        return currentSum;
    }

}
