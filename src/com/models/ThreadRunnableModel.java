package com.models;

import com.services.ParserService;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import java.util.ArrayList;
import java.util.concurrent.Callable;

/**
 * Created by Olesya on 06.04.2017.
 */
public class ThreadRunnableModel implements Runnable {

    private String filePath;

    private NumberCollection collector;

    private volatile static boolean error = false;

    private static Logger logger = Logger.getLogger(ThreadRunnableModel.class);

    static {
        DOMConfigurator.configure("log4j.xml");
    }

    public ThreadRunnableModel(String filePath, NumberCollection collector) {
        this.filePath = filePath;
        this.collector = collector;
    }

    public static void setError(boolean error) {
        ThreadRunnableModel.error = error;
    }

    @Override
    public void run() {
        System.out.println("старт потока:" + filePath);
        logger.trace("старт потока");
        ArrayList<String> words = null;
        Long currentSum = 0L;
        try {
            words = new ParserService(filePath).getNumbers();
            for (String str : words) {
                //return collector;
                if (!error) {
                    currentSum = collector.putSynchronized(str);
                } else break;
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
            ThreadRunnableModel.setError(true);
            System.out.println(e.getMessage());
        }
        System.out.println("завершение потока:" + filePath);
        logger.trace("завершение потока");
        System.out.println(currentSum);
    }
}
