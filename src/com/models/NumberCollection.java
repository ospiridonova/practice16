package com.models;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import java.util.HashMap;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Olesya on 06.04.2017.
 */
public class NumberCollection {


    private static Logger logger = Logger.getLogger(NumberCollection.class);
    static {
        DOMConfigurator.configure("log4j.xml");
    }

    private HashMap<String, Long> numbers = new HashMap<>();
    private volatile Long curSum = 0L;

    /**
     * Метод, возвращающий текущую сумму всех чисел файла - синхронизированный метод
     * @param number добавляемое число
     */
    public synchronized Long putSynchronized(String number) {
        Long currentSum = calculateSum(number);
        if (currentSum == Long.valueOf(number)) logger.trace(number + " добавлен в коллекцию");
        numbers.put(number, currentSum);
        System.out.println("Число: " + number + "(текущая сумма:" + currentSum + ")");
        return currentSum;
    }

    /**
     * Метод, возвращающий текущую сумму всех чисел файла - с использованием ReentrantLock
     * @param number добавляемое число
     */
    public Long putWithReentrantLock(String number) {
        ReentrantLock lock = new ReentrantLock();
        lock.lock();
        Long currentSum = calculateSum(number);
        if (currentSum == Long.valueOf(number)) logger.trace(number + " добавлен в коллекцию");
        numbers.put(number, currentSum);
        System.out.println("Число: " + number + "(текущая сумма:" + currentSum + ")");
        lock.unlock();
        return currentSum;
    }
    /**
     * Метод, возвращающий сумму положительных четных чисел
     * @param number число
     */
    public Long calculateSum(String number) {
        Long numLong = Long.valueOf(number);
        if (numLong % 2 == 0) {
            curSum = curSum + numLong;
        }
        return curSum;
    }
}
