package com.company;

import com.models.NumberCollection;
import com.models.ThreadModel;
import com.models.ThreadModelWithReentrantLock;
import com.models.ThreadRunnableModel;
import com.utils.UtilService;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Main {
    /**
     *
     * @param args
     */
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        NumberCollection numberCollection = new NumberCollection();
//        File f = new File("/resources/1.txt"); //можно сгенерить числа
//        f.getParentFile().mkdirs();
//        f.createNewFile();
        UtilService utilService =  new UtilService();
        utilService.generateRandomFile("resources/1.txt");
        utilService.generateRandomFile("resources/2.txt");
        String[] files = {
                "resources/1.txt",
                "resources/2.txt",
                "https://gist.githubusercontent.com/anonymous/1c2af97f93dce0420447066682d6c67d/raw/2adcce95f68d48082eb5891775ea2c86382cb199/4"

        };


        ArrayList<Future<Long>> list = new ArrayList<Future<Long>>(); //результирующий
        ExecutorService es = Executors.newFixedThreadPool(3); //якобы не должно создавать более 3 потоков)

        long start = System.currentTimeMillis();
        for(String file: files) {
            Future<Long> submit = es.submit(new ThreadModelWithReentrantLock(file, numberCollection));
            list.add(es.submit(new ThreadModelWithReentrantLock(file, numberCollection)));
        }
        es.shutdown();
        list.stream().forEach((future) -> {
            try {
                System.out.println("TOTAL:" + future.get().toString());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        });

        long finish = System.currentTimeMillis();
        System.out.println("Итоговое время ReentrantLock: "+(finish - start));
        es.shutdown();
    }
}
