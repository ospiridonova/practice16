package com.exceptions;

/**
 * Created by Olesya on 06.04.2017.
 */
public class WrongFileException extends Exception {
    public WrongFileException(String message) {
        super(message);
    }
}
