package com.utils;

import java.io.*;
import java.util.Random;

/**
 * Created by Olesya on 10.04.2017.
 * Класс вспомогательных методов
 */
public class UtilService {
    /**
     * Метод, генерирующий файл для тестирования
     * содержит числа от -5 до 5
     * в случае передачи параметра isWrong - добавление слова
     * @param pathName
     * @param isWrong
     */
    public void generateTestFile(String pathName, boolean isWrong) {
        double wantedSize = Double.parseDouble(System.getProperty("size", "1.5"));

        File file = new File(pathName);
        try {
            FileOutputStream fos = new FileOutputStream(file);
            OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");
            BufferedWriter bw = new BufferedWriter(osw);
            PrintWriter writer = new PrintWriter(bw, false);
            long start = System.currentTimeMillis();
            int counter = 0;
            String sep = "";
            if(isWrong){
                sep = "один ";
            }
            for (int number = -5; number <= 5; number++) {
                writer.print(sep);
                writer.print(number);
                sep = " ";
            }

            writer.println();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Генерация содержимого файла с переданным pathName
     * генерирует рандомные числа в диапазоне от -1000 до 1000
     * @param pathName
     */
    public void generateRandomFile(String pathName) {
        double wantedSize = Double.parseDouble(System.getProperty("size", "1.5"));
        Random random = new Random();
        File file = new File(pathName);
        try {
            FileOutputStream fos = new FileOutputStream(file);
            OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");
            BufferedWriter bw = new BufferedWriter(osw);
            PrintWriter writer = new PrintWriter(bw, false);
            //long start = System.currentTimeMillis();
            int counter = 0;
            while (++counter <= 20000) {
                for (int i = 0; i < 100; i++) {
                    new Random().ints(1000 + 1 - (-1000)).limit(100).forEach(number -> {
                        writer.print(" ");
                        writer.print(number);
                    });
                }
                writer.println();
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
